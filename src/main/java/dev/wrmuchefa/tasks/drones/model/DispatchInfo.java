package dev.wrmuchefa.tasks.drones.model;

import dev.wrmuchefa.tasks.drones.util.audit.BaseAuditor;
import jakarta.persistence.Entity;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DispatchInfo extends BaseAuditor {
//    private Drone drone;

    private String deliveryAddress;
    private double deliveryDistance;
    private double averageDeliverySpeed;
    private long averagePowerConsumedPerDelivery ;
}
