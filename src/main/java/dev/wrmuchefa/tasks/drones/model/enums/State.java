package dev.wrmuchefa.tasks.drones.model.enums;

public enum State {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}
