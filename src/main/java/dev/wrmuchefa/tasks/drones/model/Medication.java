package dev.wrmuchefa.tasks.drones.model;

import dev.wrmuchefa.tasks.drones.util.audit.BaseAuditor;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Medication extends BaseAuditor {
    @Column(columnDefinition = "VARCHAR(255) CHECK (name REGEXP '^[a-zA-Z0-9_-]+$')")
    private String name;
    private long weightInGrams;
    @Column(columnDefinition = "VARCHAR(50) CHECK (code REGEXP '^[A-Z0-9_]*$')")
    private String code;
    private String imageUri;
    private int units;
    private long totalWeight;
}
