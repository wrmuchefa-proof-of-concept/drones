package dev.wrmuchefa.tasks.drones.model.enums;

public enum Model {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}
