package dev.wrmuchefa.tasks.drones.model;

import dev.wrmuchefa.tasks.drones.model.enums.Model;
import dev.wrmuchefa.tasks.drones.model.enums.State;
import dev.wrmuchefa.tasks.drones.util.audit.BaseAuditor;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Check;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "serial_number")})
public class Drone extends BaseAuditor {
    @Column(name = "serial_number", length = 100)
    private String serialNumber;
    private Model model;
    @Column(name = "weight_limit_in_grams")
    @Check(constraints = "weight_limit_in_grams <= 500")
    private long weightLimitInGrams;
    @Column(name = "battery_capacity", columnDefinition = "integer CHECK (battery_capacity <= 100)")
    private int batteryCapacity;
    private State state;


    @OneToMany(cascade = CascadeType.ALL)
    @Builder.Default
    private Set<Medication> medicationSet = new HashSet<>();
}
