package dev.wrmuchefa.tasks.drones.repository;

import com.github.f4b6a3.ulid.Ulid;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.model.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface DroneRepository extends JpaRepository<Drone, Ulid> {
    Optional<Drone> findBySerialNumber(String serialNumber);
    Set<Drone> findByState(State state);
}
