package dev.wrmuchefa.tasks.drones.repository;

import dev.wrmuchefa.tasks.drones.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    boolean existsByName(String name);

    Optional<Medication> findByName(String name);

    Set<Medication> findByNameContainingIgnoreCase(String name);

    Set<Medication> findByNameContainingIgnoreCaseAndWeightInGramsGreaterThanEqualAndWeightInGramsLessThanEqual(String name,
                                                                                                                long minWeight,
                                                                                                                long maxWeight);

}
