package dev.wrmuchefa.tasks.drones.util.exception;

/**
 * @author Webster Rindai Muchefa, 14:38 Friday 24 September 2021.
 **/
public class AppResourceNotFoundException extends RuntimeException {
    public AppResourceNotFoundException(String message) {
        super(message);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
