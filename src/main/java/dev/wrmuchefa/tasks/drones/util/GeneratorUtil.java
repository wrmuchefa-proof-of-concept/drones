package dev.wrmuchefa.tasks.drones.util;

import com.github.f4b6a3.ulid.UlidCreator;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

@Component
public class GeneratorUtil {
    public String generateSerialNumber() {
        return UlidCreator.getMonotonicUlid().toString();
    }

    public String generateMedicationCode() { return RandomStringUtils.randomAlphanumeric(5).toUpperCase(); }
}
