package dev.wrmuchefa.tasks.drones.util.exception;

/**
 * @author Webster Rindai Muchefa, 17:01 Friday 24 September 2021.
 **/
public class AppBadRequestException extends RuntimeException {
    public AppBadRequestException(String message) {
        super(message);
    }
}
