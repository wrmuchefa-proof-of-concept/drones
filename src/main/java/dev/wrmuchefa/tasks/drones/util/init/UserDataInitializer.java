package dev.wrmuchefa.tasks.drones.util.init;

import dev.wrmuchefa.tasks.drones.model.User;
import dev.wrmuchefa.tasks.drones.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("initializeUserData")
@AllArgsConstructor
@Slf4j
public class UserDataInitializer {
    private final UserRepository repository;
    private final PasswordEncoder encoder;

    private void saveUser(String username) {
        log.info("CREATING USER WITH Username : {}", username);
        repository.save(User.builder().username(username).password(encoder.encode("secret")).build());
    }

    @PostConstruct
    private void initialise() {
        if (repository.count() < 1) {
            this.saveUser("user");
            this.saveUser("test");
        }
    }

}
