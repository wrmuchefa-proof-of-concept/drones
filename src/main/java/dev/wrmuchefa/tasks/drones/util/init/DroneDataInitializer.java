package dev.wrmuchefa.tasks.drones.util.init;

import dev.wrmuchefa.tasks.drones.dto.DroneRequest;
import dev.wrmuchefa.tasks.drones.model.enums.Model;
import dev.wrmuchefa.tasks.drones.service.DroneService;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component("initializeDroneData")
@AllArgsConstructor
public class DroneDataInitializer {
    private final DroneService service;

    private void saveDrone(Model model) {
        int weightLimit = ThreadLocalRandom.current().nextInt(40, 101) * 5;
        int batteryCapacity = ThreadLocalRandom.current().nextInt(10, 101);
        this.service.register(DroneRequest.builder()
                .batteryCapacity(batteryCapacity)
                .model(model)
                .weightLimitInGrams(weightLimit)
                .build());
    }

    @PostConstruct
    private void initialise() {
//            Preload Lightweight drone
        this.saveDrone(Model.Lightweight);

//            Preload Middleweight drone
        this.saveDrone(Model.Middleweight);

//            Preload Cruiserweight drone
        this.saveDrone(Model.Cruiserweight);

//            Preload Heavyweight drone
        this.saveDrone(Model.Heavyweight);
    }


}
