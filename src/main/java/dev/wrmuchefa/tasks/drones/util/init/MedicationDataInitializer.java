package dev.wrmuchefa.tasks.drones.util.init;


import dev.wrmuchefa.tasks.drones.dto.MedicationRequest;
import dev.wrmuchefa.tasks.drones.service.MedicationService;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component("initialiseMedicationData")
@AllArgsConstructor
public class MedicationDataInitializer {
    private final MedicationService service;

    private void saveMedication(String name, long weight) {
        this.service.add(MedicationRequest.builder()
                .imageUrl("http://testUrl")
                .name(name)
                .weightInGrams(weight)
                .build());
    }

    @PostConstruct
    private void initialise(){
        this.saveMedication("Cannabis_Sativa", 125);
        this.saveMedication("Acetaminophen", 88);
        this.saveMedication("Gamma_Hydroxybutyrate", 75);
        this.saveMedication("Guaifenesin_Syrup", 213);
        this.saveMedication("Florajen_Acidophilus", 185);
        this.saveMedication("Calcium_Citrate", 28);
        this.saveMedication("Melatonin", 501);
        this.saveMedication("Ibuprofen_Suspension", 100);
        this.saveMedication("Benefiber_Powder", 186);
        this.saveMedication("Hydralyte_Electrolyte", 397);
        this.saveMedication("Gaviscon", 10);
        this.saveMedication("Vagisil_Wash", 491);
    }

}
