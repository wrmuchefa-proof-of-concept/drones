package dev.wrmuchefa.tasks.drones.util;

import dev.wrmuchefa.tasks.drones.dto.DroneRequest;
import dev.wrmuchefa.tasks.drones.dto.MedicationRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.model.Medication;
import dev.wrmuchefa.tasks.drones.repository.DroneRepository;
import dev.wrmuchefa.tasks.drones.repository.MedicationRepository;
import dev.wrmuchefa.tasks.drones.util.exception.AppBadRequestException;
import dev.wrmuchefa.tasks.drones.util.exception.AppBusinessException;
import dev.wrmuchefa.tasks.drones.util.exception.AppResourceConflictException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class ValidationUtil {

    private final MedicationRepository medicationRepository;
    private final DroneRepository droneRepository;

    public boolean isValidMedicationRequest(MedicationRequest request) {
        if (!request.getName().matches("[a-zA-Z0-9_-]+"))
            throw new AppBadRequestException("Invalid name");
        if (request.getWeightInGrams() <= 0 || request.getWeightInGrams() > 5000)
            throw new AppBadRequestException("Invalid weight");
        if (!this.isValidURL(request.getImageUrl()))
            throw new AppBadRequestException("Invalid image url");
        if (medicationRepository.existsByName(request.getName()))
            throw new AppResourceConflictException("Medication already exist");

        return true;
    }

    public boolean isValidDroneRequest(DroneRequest request) {
        int maxDrones = 10;
        if (droneRepository.count() > maxDrones)
            throw new AppResourceConflictException("Maximum number of drones reached : ".concat(String.valueOf(maxDrones)));
        if (request.getWeightLimitInGrams() <= 0 || request.getWeightLimitInGrams() > 500)
            throw new AppBadRequestException("Invalid weight limit");

        return true;
    }

    public boolean isValidURL(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
    }

    public boolean isDroneReadyToLoad(Drone drone, Set<Medication> medicationSet) {
        if ((double) drone.getBatteryCapacity() / 100 < 0.25)
            throw new AppBusinessException("Drone battery capacity low : ".concat(String.valueOf(drone.getBatteryCapacity())));
        long totalMedicationWeight = medicationSet.stream()
                .mapToLong(Medication::getTotalWeight)
                .sum();
        if (totalMedicationWeight > drone.getWeightLimitInGrams())
            throw new AppBadRequestException("Drone weight limit reached : ".concat(String.valueOf(drone.getWeightLimitInGrams())));
        return true;
    }
}
