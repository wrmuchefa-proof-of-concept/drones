package dev.wrmuchefa.tasks.drones.config.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dev.wrmuchefa.tasks.drones.model.User;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
public class UserPrincipal implements UserDetails {
    private final String username;
    @JsonIgnore
    private final String password;

    public UserPrincipal(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static UserPrincipal build(User user) {
        return new UserPrincipal(user.getUsername(), user.getPassword());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}