package dev.wrmuchefa.tasks.drones.config;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

@Slf4j
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    @NonNull
    public Optional<String> getCurrentAuditor() {
        String username = "SYSTEM";
        try {
//            String u = SecurityContextHolder.getContext().getAuthentication().getName();
            String u = "String name";
            if (u == null)
                username = "Anonymous";
        } catch (Exception e) {
            log.warn("Error getting user from SecurityContextHolder : {}", e.getMessage());
        }
        return Optional.of(username);
    }
}
