package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.dto.DroneRequest;
import dev.wrmuchefa.tasks.drones.dto.LoadDroneRequest;
import dev.wrmuchefa.tasks.drones.dto.LoadMedicationRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.model.Medication;
import dev.wrmuchefa.tasks.drones.model.enums.State;
import dev.wrmuchefa.tasks.drones.repository.DroneRepository;
import dev.wrmuchefa.tasks.drones.util.GeneratorUtil;
import dev.wrmuchefa.tasks.drones.util.ValidationUtil;
import dev.wrmuchefa.tasks.drones.util.exception.AppResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Component
public class DroneServiceImpl implements DroneService {
    private final DroneRepository repository;
    private final GeneratorUtil generatorUtil;
    private final ValidationUtil validationUtil;
    private final MedicationService medicationService;

    @Override
    public Drone register(DroneRequest request) {
        if (this.validationUtil.isValidDroneRequest(request))
            return repository.save(Drone.builder().model(request.getModel())
                    .state(State.IDLE)
                    .weightLimitInGrams(request.getWeightLimitInGrams())
                    .serialNumber(generatorUtil.generateSerialNumber())
                    .batteryCapacity(request.getBatteryCapacity())
                    .build());
        return null;
    }

    @Override
    public Drone load(LoadDroneRequest request) {
        Drone drone = this.repository.findBySerialNumber(request.getDroneSerialNumber()).orElseThrow(
                () -> new AppResourceNotFoundException("Drone with provided serial number does not exist"));
        drone.setState(State.LOADING);
        this.repository.save(drone);
        Set<LoadMedicationRequest> cleanedMedicationRequestList = this.cleanMedicationRequestList(request.getCargoInfo());
        Set<String> nonExistingNames = new HashSet<>();

        Set<Medication> medicationSet = new HashSet<>();
        for (LoadMedicationRequest req : cleanedMedicationRequestList) {
            Medication medication = this.medicationService.findByName(req.getMedicationName()).orElse(null);
            if (medication != null) {
                medicationSet.add(Medication.builder()
                        .name(medication.getName())
                        .code(medication.getCode())
                        .weightInGrams(medication.getWeightInGrams())
                                .units(req.getUnits())
                                .totalWeight(medication.getWeightInGrams() * req.getUnits())
                        .build());
            } else {
                nonExistingNames.add(req.getMedicationName());
            }
        }

        if (!medicationSet.isEmpty()) {
            if (this.validationUtil.isDroneReadyToLoad(drone, medicationSet)) {
                drone.setMedicationSet(medicationSet);
                drone.setState(State.LOADED);
                this.repository.save(drone);
            }
        } else {
            throw new AppResourceNotFoundException("No medication found from provided list of names");
        }


        return drone;
    }

    @Override
    public Set<Drone> get(State state) {
        return repository.findByState(state);
    }

    private Set<LoadMedicationRequest> cleanMedicationRequestList(Set<LoadMedicationRequest> request) {
        Map<String, Integer> map = request.stream()
                .collect(Collectors.toMap(
                        LoadMedicationRequest::getMedicationName,
                        LoadMedicationRequest::getUnits,
                        Integer::sum));
        return map.entrySet().stream()
                .map(entry -> new LoadMedicationRequest(entry.getKey(), entry.getValue()))
                .collect(Collectors.toSet());
    }


}
