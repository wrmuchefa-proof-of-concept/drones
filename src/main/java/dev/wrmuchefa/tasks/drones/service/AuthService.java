package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.dto.LoginRequest;
import dev.wrmuchefa.tasks.drones.dto.LoginResponse;
import dev.wrmuchefa.tasks.drones.dto.SignupRequest;
import dev.wrmuchefa.tasks.drones.dto.SignupResponse;

public interface AuthService {
    LoginResponse login(LoginRequest loginRequest);

    SignupResponse register(SignupRequest request);
}
