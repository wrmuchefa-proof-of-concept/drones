package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.dto.DispatchRequest;
import dev.wrmuchefa.tasks.drones.dto.DispatchResponse;
import dev.wrmuchefa.tasks.drones.dto.LoadDroneRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;

public interface DispatchService {
    Drone load(LoadDroneRequest request);

    DispatchResponse dispatch(DispatchRequest request);
}
