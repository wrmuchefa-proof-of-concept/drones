package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.dto.MedicationRequest;
import dev.wrmuchefa.tasks.drones.model.Medication;
import dev.wrmuchefa.tasks.drones.repository.MedicationRepository;
import dev.wrmuchefa.tasks.drones.util.GeneratorUtil;
import dev.wrmuchefa.tasks.drones.util.ValidationUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class MedicationServiceImpl implements MedicationService {
    private final MedicationRepository repository;
    private final GeneratorUtil generatorUtil;
    private final ValidationUtil validationUtil;


    @Override
    public Medication add(MedicationRequest request) {
        if (validationUtil.isValidMedicationRequest(request))
            return repository.save(Medication.builder()
                    .code(generatorUtil.generateMedicationCode())
                    .name(request.getName())
                    .weightInGrams(request.getWeightInGrams())
                    .imageUri(request.getImageUrl())
                    .build());
        return null;
    }

    @Override
    public Optional<Medication> findByName(String name) {
        return this.repository.findByName(name);
    }


    @Override
    public Set<Medication> get(String name, Long minWeight, Long maxWeight) {
        return
                this.repository.findByNameContainingIgnoreCaseAndWeightInGramsGreaterThanEqualAndWeightInGramsLessThanEqual(name,
                        minWeight, maxWeight);
    }
}
