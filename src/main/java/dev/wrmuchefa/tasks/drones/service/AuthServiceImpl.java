package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.config.security.UserPrincipal;
import dev.wrmuchefa.tasks.drones.config.security.jwt.JwtUtils;
import dev.wrmuchefa.tasks.drones.dto.LoginRequest;
import dev.wrmuchefa.tasks.drones.dto.LoginResponse;
import dev.wrmuchefa.tasks.drones.dto.SignupRequest;
import dev.wrmuchefa.tasks.drones.dto.SignupResponse;
import dev.wrmuchefa.tasks.drones.model.User;
import dev.wrmuchefa.tasks.drones.repository.UserRepository;
import dev.wrmuchefa.tasks.drones.util.exception.AppAuthenticationException;
import dev.wrmuchefa.tasks.drones.util.exception.AppResourceConflictException;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            String accessToken = jwtUtils.generateToken(userPrincipal);

            return LoginResponse.builder()
                    .access_token(accessToken)
                    .message("login successful")
                    .status("success")
                    .build();
        } catch (Exception e) {
            throw new AppAuthenticationException("Incorrect username or password");
        }


    }

    @Override
    public SignupResponse register(SignupRequest request) {
        if (userRepository.existsByUsername(request.getUsername())) {
            throw new AppResourceConflictException("User already exist : " + request.getUsername());
        }
        User user = userRepository.save(User.builder()
                .username(request.getUsername())
                .password(encoder.encode(request.getPassword()))
                .build());
        return SignupResponse.builder()
                .message("User with username : '".concat(user.getUsername()).concat("' registered successfully"))
                .status("success")
                .build();
    }
}
