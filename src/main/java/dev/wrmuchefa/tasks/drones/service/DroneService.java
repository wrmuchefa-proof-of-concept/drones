package dev.wrmuchefa.tasks.drones.service;


import dev.wrmuchefa.tasks.drones.dto.DroneRequest;
import dev.wrmuchefa.tasks.drones.dto.LoadDroneRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.model.enums.State;

import java.util.Set;

public interface DroneService {
    Drone register(DroneRequest request);

    Drone load(LoadDroneRequest request);

    Set<Drone> get(State state);
}
