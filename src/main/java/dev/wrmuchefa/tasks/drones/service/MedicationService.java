package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.model.Medication;
import dev.wrmuchefa.tasks.drones.dto.MedicationRequest;

import java.util.Optional;
import java.util.Set;

public interface MedicationService {
    Medication add(MedicationRequest request);
    Optional<Medication> findByName(String name);

    Set<Medication> get(String name, Long minWeight, Long maxWeight);
}
