package dev.wrmuchefa.tasks.drones.service;

import dev.wrmuchefa.tasks.drones.dto.DispatchRequest;
import dev.wrmuchefa.tasks.drones.dto.DispatchResponse;
import dev.wrmuchefa.tasks.drones.dto.LoadDroneRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DispatchServiceImpl implements DispatchService {

    private final DroneService droneService;


    @Override
    public Drone load(LoadDroneRequest request) {
        return this.droneService.load(request);
    }

    @Override
    public DispatchResponse dispatch(DispatchRequest request) {

        return null;
    }
}
