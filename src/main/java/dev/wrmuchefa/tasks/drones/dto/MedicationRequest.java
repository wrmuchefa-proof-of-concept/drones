package dev.wrmuchefa.tasks.drones.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MedicationRequest {
    @NotNull
    private String name;

    @NotNull
    private long weightInGrams;

    @NotNull
    @NotBlank
    private String imageUrl;
}
