package dev.wrmuchefa.tasks.drones.dto;

import dev.wrmuchefa.tasks.drones.model.Medication;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DispatchResponse {
    private String droneSerialNumber;
    private String droneState;
    private Set<Medication> cargo;
    private String deliveryAddress;
    private double deliveryDistance;
    private double averageDeliverySpeed;
    private long averagePowerConsumedPerDelivery;
    private LocalDateTime loadDate;
    private LocalDateTime dispatchDate;
    private LocalDateTime deliveryDate;
    private LocalDateTime returnDate;
}
