package dev.wrmuchefa.tasks.drones.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoadDroneRequest {
    @NotNull
    @NotBlank
    private String droneSerialNumber;

    @NotNull
    @NotEmpty
    private Set<LoadMedicationRequest> cargoInfo;
}
