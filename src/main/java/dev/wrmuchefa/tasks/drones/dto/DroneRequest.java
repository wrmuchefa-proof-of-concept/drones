package dev.wrmuchefa.tasks.drones.dto;

import dev.wrmuchefa.tasks.drones.model.enums.Model;
import jakarta.validation.constraints.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DroneRequest {
    @NotNull
    private Model model;

    @NotNull
    private int weightLimitInGrams;

    @Min(value = 1, message = "Min is 1")
    @Max(value = 100,message = "Max is 100")
    private int batteryCapacity;
}
