package dev.wrmuchefa.tasks.drones.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DispatchRequest {
    @NotNull
    @NotBlank
    private String droneSerialNumber;

    @Builder.Default
    private String deliveryAddress = "The White House, 1600 Pennsylvania Avenue, N.W. Washington, DC 20500";

    @Builder.Default
    private double deliveryDistance = 28.5;

    @Builder.Default
    private double averageDeliverySpeed = 35;

    @Builder.Default
    private long averagePowerConsumedPerDelivery = 20;
}
