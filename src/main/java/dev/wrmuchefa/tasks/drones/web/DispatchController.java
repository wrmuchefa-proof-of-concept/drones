package dev.wrmuchefa.tasks.drones.web;

import dev.wrmuchefa.tasks.drones.dto.LoadDroneRequest;
import dev.wrmuchefa.tasks.drones.dto.DispatchRequest;
import dev.wrmuchefa.tasks.drones.dto.DispatchResponse;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.service.DispatchService;
import dev.wrmuchefa.tasks.drones.service.DroneService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dispatch")
@AllArgsConstructor
public class DispatchController {
    private final DispatchService service;

    @PostMapping("/load")
    public ResponseEntity<Drone> load(@Valid @RequestBody LoadDroneRequest request) {
        return ResponseEntity.ok(this.service.load(request));
    }

    @PostMapping
    public ResponseEntity<DispatchResponse> dispatch(DispatchRequest request) {
        return ResponseEntity.ok(this.service.dispatch(request));
    }
}
