package dev.wrmuchefa.tasks.drones.web;

import dev.wrmuchefa.tasks.drones.dto.DroneRequest;
import dev.wrmuchefa.tasks.drones.model.Drone;
import dev.wrmuchefa.tasks.drones.model.enums.State;
import dev.wrmuchefa.tasks.drones.service.DroneService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/drones")
@AllArgsConstructor
public class DroneController {
    private final DroneService service;

    @PostMapping("/register")
    public ResponseEntity<Drone> register(@Valid @RequestBody DroneRequest request) {
        return ResponseEntity.ok(service.register(request));
    }

    @GetMapping
    public ResponseEntity<Set<Drone>> get(@RequestParam Optional<State> state){
        return ResponseEntity.ok(this.service.get(state.orElse(State.IDLE)));
    }
}
