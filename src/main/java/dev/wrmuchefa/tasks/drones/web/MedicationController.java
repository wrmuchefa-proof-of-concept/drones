package dev.wrmuchefa.tasks.drones.web;

import dev.wrmuchefa.tasks.drones.dto.MedicationRequest;
import dev.wrmuchefa.tasks.drones.model.Medication;
import dev.wrmuchefa.tasks.drones.service.MedicationService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/medication")
@AllArgsConstructor
public class MedicationController {
    private final MedicationService service;

    @PostMapping("/add")
    public ResponseEntity<Medication> add(@Valid @RequestBody MedicationRequest request){
        return ResponseEntity.ok(service.add(request));
    }

    @GetMapping
    public ResponseEntity<Set<Medication>> get(@RequestParam Optional<String> name,
                                               @RequestParam Optional<Long> minWeight,
                                               @RequestParam Optional<Long> maxWeight){
        return ResponseEntity.ok(this.service.get(name.orElse(""), minWeight.orElse(0L), maxWeight.orElse(50000L)));
    }
}
